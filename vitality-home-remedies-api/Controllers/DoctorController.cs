using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Web;
using vitality_home_remedies_api.ViewModel;
using vitality_home_remedies_api.Model;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace vitality_home_remedies_api.Controllers
{
    [Route("api/[controller]")]
    public class DoctorController : Controller
    {
        readonly DatabaseContext _dbcontext;
        public IConfiguration _config { get; set; }
        public DoctorController(DatabaseContext databaseContext)
        {
            _dbcontext = databaseContext;
        }
        [HttpGet]
        public IActionResult Get(string id)
        {
            var user = _dbcontext.Doctor.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
            return Ok(user);
        }
        [HttpGet]
        [Route("AllUsers")]
        public IActionResult Get()
        {
            var users = _dbcontext.Doctor.Where(x => x.IsActive == true).ToList();
            return Ok(users);
        }
        [HttpGet]
        [Route("Phone/{phone}")]
        public IActionResult GetAction([FromRoute] string phone)
        {
            if (phone == "null")
            {
                return BadRequest("email is null");
            }
            var user = _dbcontext.Doctor.Where(x => x.phonenumber == phone).FirstOrDefault();
            return Ok(user);
        }
        [HttpPost]
        public IActionResult Post([FromBody] DoctorViewModel person)
        {
            try
            {
                if (person.Id != null)
                {
                    return Ok(Update(person));
                }

                var personInfo = new Doctor
                {
                    Name = person.Name,
                    LastName = person.LastName,
                    Email = person.Email,
                    Specialisation = person.Specialisation,
                    phonenumber = person.phonenumber,
                    CreatedDate = DateTime.Now,
                    CreatedUser = person.Name,
                    IsActive = true


                };
                _dbcontext.Add(personInfo);
                _dbcontext.SaveChanges();
                return Ok(personInfo.Id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }

        }

        private IActionResult Update(DoctorViewModel person)
        {
            var user = _dbcontext.Doctor.Where(x => x.Id == Guid.Parse(person.Id)).FirstOrDefault();

            user.Name = person.Name;
            user.LastName = person.LastName;
            user.Email = person.Email;
            user.Specialisation = person.Specialisation;
            user.phonenumber = person.phonenumber;
            user.ModifiedDate = DateTime.Now;
            user.ModifiedUser = person.Name;

            _dbcontext.Entry(user).State = EntityState.Modified;
            _dbcontext.SaveChanges();
            return Ok(user);
        }
        [HttpDelete]
        [Route("Delete/{email}")]
        public IActionResult Delete([FromRoute] string email)
        {
            var user = _dbcontext.Doctor.Where(x => x.Email == email).FirstOrDefault();
            _dbcontext.Remove(user);
            _dbcontext.SaveChanges();
            return Ok();
        }
        [HttpPost]
        [Route("Request")]
        public IActionResult PostRequest([FromBody] RequestDoctor person)
        {
            try
            {
                var user = _dbcontext.UserReg.Where(x => x.Email == person.Email).FirstOrDefault();
                var doctor = _dbcontext.Doctor.Where(x => x.Email == person.DoctorEmail).FirstOrDefault();
                bool isSend = false;
                
                   isSend = SendRequestEmail(person, doctor);
                if(isSend == true){
                    //Booking save

                var personInfo = new Booking
                {
                    CreatedDate = DateTime.Now,
                    Doctor_Id = doctor.Id.ToString(),
                    UserId = user.Id.ToString()
                };
                _dbcontext.Add(personInfo);
                _dbcontext.SaveChanges();

                    return Ok("Email sent");

                }

                return Ok("Email not sent");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }

        }
        private bool SendRequestEmail(RequestDoctor user, Doctor doctor)
        {
            try
            {
                
                    MailMessage mail = new MailMessage();
                    SmtpClient client = new SmtpClient("smtp.gmail.com");
                    client.Port = 587;
                    mail.To.Add(doctor.Email);
                    mail.From = new MailAddress("noreply@gmail.com");
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.EnableSsl = true;

                    client.Credentials = new System.Net.NetworkCredential("gregorytlhotse@gmail.com", "Gregory1994&"); //Turn Less secure app access ON using Gmail    
                    mail.Subject = "Home Remedies Request";

                    string body = "Hi " + doctor.Name + ",|\r\nYou have a request from: " + user.FullName + " and they need help!<br /><br />" + user.FullName + " message:<br />" + user.Message + "|<br /><br /><u><b>Contact details<b/></u> " +
                    "|<br /><br />Email: " + user.Email + "|<br />Mobile Number: " + user.MobileNumber + "|<br /><br /><br /><b>Thank you|\r\n\nHome Remedies Team|\n\nWhatsapp Support: +2781 430 7563<b/>";
                    body = body.Replace("|", System.Environment.NewLine);
                    mail.Body = body;
                    mail.IsBodyHtml = true;
                    client.Send(mail);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private bool CompareSentence(string s1, string s2)
        {

            string normalized1 = Regex.Replace(s1, @"\s", "");
            string normalized2 = Regex.Replace(s2, @"\s", "");
            bool stringEquals = String.Equals(
                normalized1,
                normalized2,
                StringComparison.OrdinalIgnoreCase);
            Console.WriteLine(stringEquals);
            return stringEquals;
        }
        public static int Compute(string s, string t)
        {


            if (string.IsNullOrEmpty(s))
            {
                if (string.IsNullOrEmpty(t))
                    return 0;
                return t.Length;
            }

            if (string.IsNullOrEmpty(t))
            {
                return s.Length;
            }

            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // initialize the top and right of the table to 0, 1, 2, ...
            for (int i = 0; i <= n; d[i, 0] = i++) ;
            for (int j = 1; j <= m; d[0, j] = j++) ;

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
                    int min1 = d[i - 1, j] + 1;
                    int min2 = d[i, j - 1] + 1;
                    int min3 = d[i - 1, j - 1] + cost;
                    d[i, j] = Math.Min(Math.Min(min1, min2), min3);
                }
            }
            return d[n, m];
        }





    }




}

