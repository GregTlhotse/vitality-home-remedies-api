using vitality_home_remedies_api.ViewModel;
using vitality_home_remedies_api.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;

namespace vitality_home_remedies_api.Controllers
{

    [Route("api/[controller]")]
    public class LanguageController:Controller
    {
        readonly DatabaseContext _dbcontext;
        public IConfiguration _config { get; set; }
        public LanguageController(DatabaseContext databaseContext){
            _dbcontext = databaseContext;
        }
        [HttpGet]
        public IActionResult Get(string id){
            var user = _dbcontext.ProdLanguage.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
            return Ok(user);
        }
        [HttpGet]
        [Route("AllLanguages")]
        public IActionResult Get() {
            var users = _dbcontext.ProdLanguage.Where(x => x.IsActive == true).ToList();
            return Ok(users);
        }
        [HttpPost]
         public IActionResult Post([FromBody] LanguageViewModel language)
        {
            try{
            
				if(language.Id != null) {
					var usrCheck = _dbcontext.ProdLanguage.Where(x => x.Id == Guid.Parse(language.Id)).Count();

				if(usrCheck>0){
					return Ok(Update(language));
				}
				}
            

            var languageInfo = new ProdLanguage{
                Name = language.Name,
                Description = language.Description,
                CreatedDate = DateTime.Now, 
				CreatedUser = "Admin",
			    IsActive = true
			   

            }; 
            _dbcontext.Add(languageInfo);
            _dbcontext.SaveChanges();
            return Ok(languageInfo.Id);
            }catch(Exception ex){
                return BadRequest(ex.Message);

            }

        }

		private IActionResult Update(LanguageViewModel language) {
		  var user = _dbcontext.ProdLanguage.Where(x => x.Id == Guid.Parse(language.Id)).FirstOrDefault();

			user.Name = language.Name;
            user.Description = language.Description;
			user.ModifiedDate = DateTime.Now;
			user.ModifiedUser = "Admin";
 
		   _dbcontext.Entry(user).State = EntityState.Modified;
		   _dbcontext.SaveChanges();
		   return Ok(user);
		}
        [HttpDelete]
		[Route("Delete/{id}")]
		 public IActionResult Delete([FromRoute]string id) {
			 		  var user = _dbcontext.ProdLanguage.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
					   _dbcontext.Remove(user);
					   _dbcontext.SaveChanges();
					   return Ok();
		 }



    }
}
 