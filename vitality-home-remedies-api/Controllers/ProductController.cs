using vitality_home_remedies_api.ViewModel;
using vitality_home_remedies_api.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;

namespace vitality_home_remedies_api.Controllers
{

    [Route("api/[controller]")]
    public class ProductController:Controller
    {
        readonly DatabaseContext _dbcontext;
        public IConfiguration _config { get; set; }
        public ProductController(DatabaseContext databaseContext){
            _dbcontext = databaseContext;
        }
        [HttpGet]
        public IActionResult Get(string id){
            var user = _dbcontext.Product.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
            return Ok(user);
        }
        [HttpGet]
        [Route("AllProducts")]
        public IActionResult Get() {
            var users = _dbcontext.Product.Where(x => x.IsActive == true).ToList();
            return Ok(users);
        }
        [HttpPost]
         public IActionResult Post([FromBody] ProductViewModel product)
        {
            try{
            
				if(product.Id != null) {
					var usrCheck = _dbcontext.Product.Where(x => x.Id == Guid.Parse(product.Id)).Count();

				if(usrCheck>0){
					return Ok(Update(product));
				}
				}
            

            var languageInfo = new Product{
                Name = product.Name,
                Description = product.Description,
                CreatedDate = DateTime.Now, 
				CreatedUser = "Admin",
			    IsActive = true
			   

            }; 
            _dbcontext.Add(languageInfo);
            _dbcontext.SaveChanges();
            return Ok(languageInfo.Id);
            }catch(Exception ex){
                return BadRequest(ex.Message);

            }

        }

		private IActionResult Update(ProductViewModel product) {
		  var user = _dbcontext.Product.Where(x => x.Id == Guid.Parse(product.Id)).FirstOrDefault();

			user.Name = product.Name;
            user.Description = product.Description;
			user.ModifiedDate = DateTime.Now;
			user.ModifiedUser = "Admin";
 
		   _dbcontext.Entry(user).State = EntityState.Modified;
		   _dbcontext.SaveChanges();
		   return Ok(user);
		}
        [HttpDelete]
		[Route("Delete/{id}")]
		 public IActionResult Delete([FromRoute]string id) {
			 		  var user = _dbcontext.Product.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
					   _dbcontext.Remove(user);
					   _dbcontext.SaveChanges();
					   return Ok();
		 }



    }
}
 