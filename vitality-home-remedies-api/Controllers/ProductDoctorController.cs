using vitality_home_remedies_api.ViewModel;
using vitality_home_remedies_api.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;

namespace vitality_home_remedies_api.Controllers
{

    [Route("api/[controller]")]
    public class ProductDoctorController:Controller
    {
        readonly DatabaseContext _dbcontext;
        public IConfiguration _config { get; set; }
        public ProductDoctorController(DatabaseContext databaseContext){
            _dbcontext = databaseContext;
        }
        [HttpGet]
        public IActionResult Get(string id){
            var user = _dbcontext.Product_Linked_Doctor.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
            return Ok(user);
        }
        [HttpGet]
        [Route("AllProductDoctors")]
        public IActionResult Get() {


            var query = (from p in _dbcontext.Product
                                join pd in _dbcontext.Product_Linked_Doctor on p.Id equals Guid.Parse(pd.Product_Id)
                                join d in _dbcontext.Doctor on  Guid.Parse(pd.Doctor_Id) equals d.Id
                         select new
                         {
                             p,
                             d,
                             pd
                         }).ToList();

            return Ok(query);




          

        }
        [HttpPost]
         public IActionResult Post([FromBody] ProductDoctorViewModel product)
        {
            try{
            
				if(product.Id != null) {
					var usrCheck = _dbcontext.Product_Linked_Doctor.Where(x => x.Id == Guid.Parse(product.Id)).Count();

				if(usrCheck>0){
					return Ok(Update(product));
				}
				}
            

            var languageInfo = new ProductDoctor{
                Product_Id = product.Product_Id,
                 Doctor_Id = product.Doctor_Id,
                CreatedDate = DateTime.Now, 
				CreatedUser = "Admin",
			    IsActive = true
			   

            }; 
            _dbcontext.Add(languageInfo);
            _dbcontext.SaveChanges();
            return Ok(languageInfo.Id);
            }catch(Exception ex){
                return BadRequest(ex.Message);

            }

        }

		private IActionResult Update(ProductDoctorViewModel product) {
		  var user = _dbcontext.Product_Linked_Doctor.Where(x => x.Id == Guid.Parse(product.Id)).FirstOrDefault();

			user.Product_Id = product.Product_Id;
            user.Doctor_Id = product.Doctor_Id;
			user.ModifiedDate = DateTime.Now;
			user.ModifiedUser = "Admin";
 
		   _dbcontext.Entry(user).State = EntityState.Modified;
		   _dbcontext.SaveChanges();
		   return Ok(user);
		}
        [HttpDelete]
		[Route("Delete/{id}")]
		 public IActionResult Delete([FromRoute]string id) {
			 		  var user = _dbcontext.Product_Linked_Doctor.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
					   _dbcontext.Remove(user);
					   _dbcontext.SaveChanges();
					   return Ok();
		 }



    }
}
 