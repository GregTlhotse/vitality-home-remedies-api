using vitality_home_remedies_api.ViewModel;
using vitality_home_remedies_api.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace vitality_home_remedies_api.Controllers
{

    [Route("api/[controller]")]
    public class ProductLanguageController : Controller
    {
        readonly DatabaseContext _dbcontext;
        public IConfiguration _config { get; set; }
        public ProductLanguageController(DatabaseContext databaseContext)
        {
            _dbcontext = databaseContext;
        }
        [HttpGet]
        public IActionResult Get(string id)
        {
            var user = _dbcontext.Product_Linked_Language.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
            return Ok(user);
        }
        [HttpGet]
        [Route("AllProductLanguages")]
        public IActionResult Get()
        {


            var query = (from p in _dbcontext.Product
                         join pl in _dbcontext.Product_Linked_Language on p.Id equals Guid.Parse(pl.Product_Id)
                         join l in _dbcontext.ProdLanguage on Guid.Parse(pl.Language_Id) equals l.Id
                         select new
                         {
                             p,
                             l,
                             pl
                         }).ToList();

            return Ok(query);






        }
        [HttpGet]
        [Route("GetSearched/{search}")]
        public IActionResult GetSearched([FromRoute] string search)
        {
            try
            {
                string phrase = search;
                string[] words = phrase.Split(' ');
                List<ProductLanguage> searchedData = new List<ProductLanguage>();

                foreach (var word in words)
                {
                    System.Console.WriteLine(word);
                    if (word.Length > 3)
                    {
                        SqlParameter para = new SqlParameter("@symptom", "%" + word + "%");
                        var data = _dbcontext.Product_Linked_Language.FromSql($"select * from dbo.Product_Linked_Language where symptom LIKE @symptom", para).FirstOrDefault();
                        searchedData.Add(data);
                    }

                }

                List<SearchedData> searched = null;
                searchedData.ForEach(x =>
                {
                    if (x != null)
                    {
                         searched = (from p in _dbcontext.Product
                                     join pl in _dbcontext.Product_Linked_Language on p.Id equals Guid.Parse(pl.Product_Id)
                                     join l in _dbcontext.ProdLanguage on Guid.Parse(pl.Language_Id) equals l.Id
                                     join pd in _dbcontext.Product_Linked_Doctor on Guid.Parse(pl.Product_Id) equals Guid.Parse(pd.Product_Id)
                                     join d in _dbcontext.Doctor on Guid.Parse(pd.Doctor_Id) equals d.Id
                                     where pl.Id == x.Id
                                     select new SearchedData
                                     {
                                         Product = p,
                                         Language = l,
                                         ProductLanguage = pl,
                                         ProductDoctor = pd,
                                         Doctor = d
                                     }).Distinct().ToList();

                    }

                });

                return Ok(searched);

            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }


            //     List<ProdLanguage> languages = new List<ProdLanguage>();
            //     List<SearchedData> searched = new List<SearchedData>();
            //     var query = (from p in _dbcontext.Product
            //                  join pl in _dbcontext.Product_Linked_Language on p.Id equals Guid.Parse(pl.Product_Id)
            //                  join l in _dbcontext.ProdLanguage on Guid.Parse(pl.Language_Id) equals l.Id
            //                  join pd in _dbcontext.Product_Linked_Doctor on Guid.Parse(pl.Product_Id) equals Guid.Parse(pd.Product_Id)
            //                  join d in _dbcontext.Doctor on Guid.Parse(pd.Doctor_Id) equals d.Id
            //                  select new SearchedData
            //                  {
            //                      Product = p,
            //                      Language = l,
            //                      ProductLanguage = pl,
            //                      ProductDoctor = pd,
            //                      Doctor = d
            //                  }).ToList();
            //                 //  query.Where(x => DbFunctions.Like(x.ProductLanguage.Symptom, string.Format("%{0}%",search)))

            //     query.ForEach(x =>
            // {
            //     string a = x.ProductLanguage.Symptom;
            //     string b = search;
            //     bool check = x.ProductLanguage.Symptom.Contains(search);
            //     if (check)
            //     {
            //         Console.WriteLine("This is true check");
            //     }
            //     a = Regex.Replace(a, @"\s", "");
            //     b = Regex.Replace(b, @"\s", "");
            //     Regex r = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            //     a = r.Replace(a, String.Empty);
            //     b = r.Replace(b, String.Empty);
            //     int tempA = 0;
            //     int tempB = 0;
            //     if (a.Length > b.Length)
            //     {
            //         tempA = a.Length - b.Length;
            //     }
            //     if (b.Length > a.Length)
            //     {
            //         tempB = b.Length - a.Length;
            //     }
            //     if (tempA < Compute(a, b) || tempB < Compute(a, b))
            //     {
            //         Console.WriteLine("**********TRUE*********** " + Compute(a, b));
            //         Console.WriteLine("Compute = " + Compute(a, b));
            //         Console.WriteLine("tempA = " + tempA);
            //         Console.WriteLine("tempB = " + tempB);
            //         searched.Add(x);
            //     }
            //     else
            //     {
            //         Console.WriteLine("**********FALSE*********** " + Compute(a, b));
            //         Console.WriteLine("Compute = " + Compute(a, b));
            //         Console.WriteLine("tempA = " + tempA);
            //         Console.WriteLine("tempB = " + tempB);
            //     }
            // });

            //     return Ok(searched);






        }
        [HttpPost]
        public IActionResult Post([FromBody] ProductLanguageViewModel product)
        {
            try
            {

                if (product.Id != null)
                {
                    var usrCheck = _dbcontext.Product_Linked_Language.Where(x => x.Id == Guid.Parse(product.Id)).Count();

                    if (usrCheck > 0)
                    {
                        return Ok(Update(product));
                    }
                }


                var languageInfo = new ProductLanguage
                {
                    Product_Id = product.Product_Id,
                    Language_Id = product.Language_Id,
                    Description = product.Description,
                    Symptom = product.Symptom,
                    CreatedDate = DateTime.Now,
                    CreatedUser = "Admin",
                    IsActive = true


                };
                _dbcontext.Add(languageInfo);
                _dbcontext.SaveChanges();
                return Ok(languageInfo.Id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }

        }

        private IActionResult Update(ProductLanguageViewModel product)
        {
            var user = _dbcontext.Product_Linked_Language.Where(x => x.Id == Guid.Parse(product.Id)).FirstOrDefault();

            user.Product_Id = product.Product_Id;
            user.Language_Id = product.Language_Id;
            user.Description = product.Description;
            user.ModifiedDate = DateTime.Now;
            user.ModifiedUser = "Admin";

            _dbcontext.Entry(user).State = EntityState.Modified;
            _dbcontext.SaveChanges();
            return Ok(user);
        }
        [HttpDelete]
        [Route("Delete/{id}")]
        public IActionResult Delete([FromRoute] string id)
        {
            var user = _dbcontext.Product_Linked_Language.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
            _dbcontext.Remove(user);
            _dbcontext.SaveChanges();
            return Ok();
        }




    }
}
