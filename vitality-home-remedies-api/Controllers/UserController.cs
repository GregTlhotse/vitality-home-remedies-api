using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Web;
using vitality_home_remedies_api.ViewModel;
using vitality_home_remedies_api.Model;
using System.Collections.Generic;

namespace vitality_home_remedies_api.Controllers
{
    [Route("api/[controller]")]
    public class UserController:Controller
    {
readonly DatabaseContext _dbcontext;
        public IConfiguration _config { get; set; }
        public UserController(DatabaseContext databaseContext){
            _dbcontext = databaseContext;
        }
        [HttpGet]
        public IActionResult Get(string id){
            var user = _dbcontext.UserReg.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
            return Ok(user);
        }
        [HttpGet]
        [Route("AllUsers")]
        public IActionResult Get() {
            var users = _dbcontext.UserReg.Where(x => x.IsActive == true).ToList();
            return Ok(users);
        }
		[HttpGet]
		[Route("Phone/{phone}")]
        public IActionResult GetAction([FromRoute]string phone){
			if(phone == "null") {
                return BadRequest("email is null");
            }
            var user = _dbcontext.UserReg.Where(x => x.phonenumber == phone).FirstOrDefault();
			user.Password = null;
            return Ok(user);
        }
        [HttpPost]
        public IActionResult Post([FromBody] UserViewModel person)
        {
            try{
            var user = _dbcontext.UserReg.Where(x => x.Email == person.Email.Trim()).Count();
            if(user>0){
				if(person.Id != null) {
					var usrCheck = _dbcontext.UserReg.Where(x => x.Id == Guid.Parse(person.Id)).Count();

				if(usrCheck>0){
					return Ok(Update(person));
				}
				}
                return Ok("Email already exist");
            }

            byte[] hashedPassword = EncryptPassword(person.Password);
            var personInfo = new User{
                FirstName = person.FirstName,
                LastName = person.LastName,
				IdNumber = person.IdNumber,
                Email = person.Email,
                phonenumber = person.phonenumber,
                Password = hashedPassword,
                CreatedDate = DateTime.Now, 
				CreatedUser = person.FirstName,
			    IsActive = true
			   

            }; 
            _dbcontext.Add(personInfo);
            _dbcontext.SaveChanges();
            return Ok(personInfo.Id);
            }catch(Exception ex){
                return BadRequest(ex.Message);

            }

        }

		private IActionResult Update(UserViewModel person) {
		  var user = _dbcontext.UserReg.Where(x => x.Id == Guid.Parse(person.Id)).FirstOrDefault();

			user.FirstName = person.FirstName;
			user.LastName = person.LastName;
			user.Email = person.Email;
			user.IdNumber = person.IdNumber;
			user.phonenumber = person.phonenumber;
			user.ModifiedDate = DateTime.Now;
			user.ModifiedUser = person.FirstName;
 
		   _dbcontext.Entry(user).State = EntityState.Modified;
		   _dbcontext.SaveChanges();
		   return Ok(user);
		}

    [HttpPost]
		[Route("Login")]
		public IActionResult PostUserLogin([FromBody] UserViewModel value)
		{

			try
			{
				byte[] hashedPassword = EncryptPassword(value.Password);
				var user = _dbcontext.UserReg.Where(x => x.Email == value.Email && x.IsActive == true).FirstOrDefault();
				
				if (user == null)
				{
					return NotFound("User not found");
				}

				bool passwordsMatch = CompareBytes(user.Password, hashedPassword);
				if(passwordsMatch == false) {
					return NotFound("Incorrect Password");
				}
				
				var claims = new[]
				{
							new Claim(ClaimTypes.Name, user.Email),
							new Claim(ClaimTypes.Email, user.Email)
							 };


				var token = new JwtSecurityToken(

					expires: DateTime.Now.AddMonths(6));
					    user.Password = null;
                user.PasswordResetCode = null;
                user.PasswordResetTime = null;

				return Ok(new
				{
					user,
					token = new JwtSecurityTokenHandler().WriteToken(token),
				});

			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return BadRequest("Username and Password Not found, Please sign-up first");
			}

		 }
         byte[] EncryptPassword(string password)
		{
			byte[] data = System.Text.Encoding.ASCII.GetBytes(password);
			MD5 md5 = MD5.Create();
			byte[] hash = md5.ComputeHash(data);
			return hash;
		}
        		bool CompareBytes(byte[] databasePassword, byte[] userPassword)
		{
			bool bEqual = false;
			if (userPassword.Length == databasePassword.Length)
			{
				int i = 0;
				while ((i < userPassword.Length) && (userPassword[i] == databasePassword[i]))
				{
					i += 1;
				}
				if (i == userPassword.Length)
				{
					bEqual = true;
				}
			}
			return bEqual;
		}
		[HttpDelete]
		[Route("Delete/{email}")]
		 public IActionResult Delete([FromRoute]string email) {
			 		  var user = _dbcontext.UserReg.Where(x => x.Email == email).FirstOrDefault();
					   _dbcontext.Remove(user);
					   _dbcontext.SaveChanges();
					   return Ok();
		 }
		 [HttpGet]
        [Route("GeneratePasswordCode/{email}")]
        // [AllowAnonymous]
        public async Task<IActionResult> GeneratePasswordCode([FromRoute]string email)
        {

            var user = await _dbcontext.UserReg.Where
              (x => x.Email == email).FirstOrDefaultAsync();

            if (user.Id.ToString() != "")
            {
                try
                {
                    Random generator = new Random();
                    String resetCode = generator.Next(0, 999999).ToString("D6");
                    user.PasswordResetCode = resetCode;
                    user.PasswordResetTime = DateTime.Now;
                    _dbcontext.Entry(user).State = EntityState.Modified;
                    await _dbcontext.SaveChangesAsync();
                    //send email with code
                    if (SendEmail(user, resetCode))
                    {
                        return Ok(user);
                    }
                    else
                    {
                        return BadRequest("Error in sending an email with code, please contact support on +27 72 734 0800");
                    }

                }
                catch (Exception)
                {
                    return BadRequest("Failed to generate password");
                }

            }
            return BadRequest("User not found, please ensure you have entered correct details");

        }

        [HttpPost]
        [Route("ResetPassword")]
        //   [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromBody] ForgotPasswordViewModel forgotPasswordViewModel)
        {


            var user = await _dbcontext.UserReg.Where
              (x => x.Email == forgotPasswordViewModel.Email.Trim()
              && x.PasswordResetCode == forgotPasswordViewModel.PasswordResetCode.Trim()
              && x.PasswordResetTime.Value.AddMinutes(180) > DateTime.Now
              ).FirstOrDefaultAsync();

            var hashedPassword = EncryptPassword(forgotPasswordViewModel.NewPassword);

            try
            {
                if (user.Id.ToString() != "")
                {
                    user.Password = hashedPassword;
                    user.PasswordResetCode = "";
                    user.PasswordResetTime = null;
                    user.ModifiedDate = DateTime.Now;
                    user.ModifiedUser = user.FirstName;
                    
                    _dbcontext.Entry(user).State = EntityState.Modified;
                    await _dbcontext.SaveChangesAsync();
                }
                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }

        }
		 private bool SendEmail(User user, string passwordResetCode)
        {
            try
            {

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient("smtp.gmail.com");
                client.Port = 587;
                mail.To.Add(user.Email);
                mail.From = new MailAddress("noreply@gmail.com");
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.EnableSsl = true;

                client.Credentials = new System.Net.NetworkCredential("gregorytlhotse@gmail.com", "Gregory1994&");
                mail.Subject = "Home Remedies Password Reset.";
                string body = "Hi " + user.FirstName + ",|\r\n<br /><br />Your password reset code is: " + passwordResetCode + " and is valid up to " + DateTime.Now.AddMinutes(180) + ".|\r\n\nGo to the forgot password reset <a href=http://localhost:4200/reset/password/"+user.Email+">page</a> and " +
                "enter the reset code together with the new password.|\r\n\n\n<br /><br /><br />Thank you|\r\n\nHome Remedies Team|\n\nWhatsapp Support: +2772 734 0800";
                body = body.Replace("|", System.Environment.NewLine);
                mail.Body = body;
                mail.IsBodyHtml = true;
                client.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
	



    }




}

