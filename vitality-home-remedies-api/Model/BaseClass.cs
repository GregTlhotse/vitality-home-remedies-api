using Microsoft.EntityFrameworkCore;
using vitality_home_remedies_api.Model;
using System;

namespace vitality_home_remedies_api.Model{
    public class BaseClass{
        public Guid Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }
        public bool IsActive  { get; set; }


    }
}
