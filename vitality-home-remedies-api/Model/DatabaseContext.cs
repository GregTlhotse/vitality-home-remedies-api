
using Microsoft.EntityFrameworkCore;
namespace vitality_home_remedies_api.Model{
    public class DatabaseContext:DbContext{
        public DatabaseContext(DbContextOptions<DatabaseContext> options): base(options)
        {
            
        }
        
        public DbSet<User> UserReg { get; set; }
        public DbSet<Doctor> Doctor { get; set; }
        public DbSet<ProdLanguage> ProdLanguage { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductLanguage> Product_Linked_Language { get; set; }
        public DbSet<ProductDoctor> Product_Linked_Doctor { get; set; }
        public DbSet<Booking> Booking { get; set; }






    }
}
