
using Microsoft.EntityFrameworkCore;
using System;
using vitality_home_remedies_api.Model;

namespace vitality_home_remedies_api.Model{
    public class Doctor:BaseClass{
        public  string Name { get; set; }
         public  string LastName { get; set; }
         public  string Email { get; set; }
         public  string phonenumber { get; set; }
         public  string Specialisation { get; set; }

    }
}
