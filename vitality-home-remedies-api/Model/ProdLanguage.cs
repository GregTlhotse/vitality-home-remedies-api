
using Microsoft.EntityFrameworkCore;
using System;
using vitality_home_remedies_api.Model;

namespace vitality_home_remedies_api.Model{
    public class ProdLanguage:BaseClass{
        public  string Name { get; set; }
         public  string Description { get; set; }
   
    }
}
