
using Microsoft.EntityFrameworkCore;
using System;
using vitality_home_remedies_api.Model;

namespace vitality_home_remedies_api.Model{
    public class ProductLanguage:BaseClass{
        public  string Product_Id { get; set; }
         public  string Language_Id { get; set; }
         public  string Description { get; set; }//Symptoms
        public  string Symptom { get; set; }//Symptoms
         
   
    }
}
