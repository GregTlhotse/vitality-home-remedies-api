
using Microsoft.EntityFrameworkCore;
using System;
using vitality_home_remedies_api.Model;

namespace vitality_home_remedies_api.Model{
    public class User: BaseClass{
        public  string FirstName { get; set; }
         public  string LastName { get; set; }
         public  string Email { get; set; }
         public  string phonenumber { get; set; }

         public  byte[] Password { get; set; }
         public  string IdNumber { get; set; }
         public string PasswordResetCode { get; set; }
         public DateTime? PasswordResetTime { get; set; }

    }
}
