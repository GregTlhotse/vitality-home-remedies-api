
CREATE TABLE UserReg (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    CreatedDate DATETIME,
    CreatedUser  varchar(255),
    ModifiedDate  DATETIME,
    ModifiedUser varchar(255),
    FirstName varchar(255),
    LastName varchar(255),
    IdNumber varchar(255),
    Email varchar(255),
    phonenumber varchar(255),
    Password varbinary(max),
    IsActive BIT,
    PasswordResetCode varchar(255),
    PasswordResetTime DATETIME
);5
go
-- Insert Admin Info
--Use swagger To Insert data:

--   "FirstName": "Admin",
--   "LastName": "Admin",
--   "Email": "admin@remedies.com",
--   "Password": "admin2020"

go
CREATE TABLE Doctor (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    CreatedDate DATETIME,
    CreatedUser  varchar(255),
    ModifiedDate  DATETIME,
    ModifiedUser varchar(255),
    IsActive BIT,
    Name varchar(255),
    LastName varchar(255),
    Specialisation varchar(255),
    Email varchar(255),
    phonenumber varchar(255),

);
go
CREATE TABLE Booking (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    CreatedDate DATETIME,
    UserId varchar(255),
    Doctor_Id varchar(255),

);
go
go
CREATE TABLE ProdLanguage (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    CreatedDate DATETIME,
    CreatedUser  varchar(255),
    ModifiedDate  DATETIME,
    ModifiedUser varchar(255),
    IsActive BIT,
     Name varchar(255),
    Description varchar(255),

);
go
CREATE TABLE Product (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    CreatedDate DATETIME,
    CreatedUser  varchar(255),
    ModifiedDate  DATETIME,
    ModifiedUser varchar(255),
    IsActive BIT,
     Name varchar(255),
    Description varchar(max),

);
go
CREATE TABLE Product_Linked_Language (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    CreatedDate DATETIME,
    CreatedUser  varchar(255),
    ModifiedDate  DATETIME,
    ModifiedUser varchar(255),
    IsActive BIT,
     Product_Id varchar(255),
     Language_Id varchar(255),
    Description varchar(max),
    Symptom varchar(max),

);
go
CREATE TABLE Product_Linked_Doctor (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    CreatedDate DATETIME,
    CreatedUser  varchar(255),
    ModifiedDate  DATETIME,
    ModifiedUser varchar(255),
    IsActive BIT,
     Product_Id varchar(255),
     Doctor_Id varchar(255),
);
go
CREATE TABLE Request_Doctor (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    CreatedDate DATETIME,
    CreatedUser  varchar(255),
    ModifiedDate  DATETIME,
    ModifiedUser varchar(255),
    IsActive BIT,
     FullName varchar(255),
     Email varchar(255),
     MobileNumber varchar(255),
     Message varchar(max),
);
go



