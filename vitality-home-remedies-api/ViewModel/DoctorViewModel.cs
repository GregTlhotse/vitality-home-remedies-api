
using Microsoft.EntityFrameworkCore;
using System;

namespace vitality_home_remedies_api.ViewModel{
    public class DoctorViewModel{
        public  string Id { get; set; }
        public  string Name { get; set; }
         public  string LastName { get; set; }
         public  string Email { get; set; }
         public  string phonenumber { get; set; }
        public  string Specialisation { get; set; }

    }
}
