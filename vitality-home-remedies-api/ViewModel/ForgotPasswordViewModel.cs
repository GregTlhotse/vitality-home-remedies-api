using Microsoft.EntityFrameworkCore;
using System;

namespace vitality_home_remedies_api.ViewModel{
    public class ForgotPasswordViewModel{
        public string Email { get; set; }
         public  string NewPassword { get; set; }
         public string PasswordResetCode { get; set; }


    }
}
