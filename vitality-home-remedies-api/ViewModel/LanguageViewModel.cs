
using Microsoft.EntityFrameworkCore;
using System;

namespace vitality_home_remedies_api.ViewModel{
    public class LanguageViewModel{
        public  string Id { get; set; }
        public  string Name { get; set; }
        public  string Description { get; set; }

    }
}
