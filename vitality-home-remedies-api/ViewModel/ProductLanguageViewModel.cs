
using Microsoft.EntityFrameworkCore;
using System;

namespace vitality_home_remedies_api.ViewModel{
    public class ProductLanguageViewModel{
        public  string Id { get; set; }
       public  string Product_Id { get; set; }
         public  string Language_Id { get; set; }
         public  string Description { get; set; }
         public  string Symptom { get; set; }

    }
}
