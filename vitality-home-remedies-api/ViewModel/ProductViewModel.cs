
using Microsoft.EntityFrameworkCore;
using System;

namespace vitality_home_remedies_api.ViewModel{
    public class ProductViewModel{
        public  string Id { get; set; }
        public  string Name { get; set; }
        public  string Description { get; set; }
    

    }
}
