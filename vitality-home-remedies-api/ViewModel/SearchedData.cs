
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using vitality_home_remedies_api.Model;

namespace vitality_home_remedies_api.ViewModel{
    public class SearchedData{
        public  Product Product { get; set; }
        public  ProdLanguage Language { get; set; }
        public  ProductLanguage ProductLanguage { get; set; }
        public  ProductDoctor ProductDoctor { get; set; }
        public  Doctor Doctor { get; set; }

    

    }
}
