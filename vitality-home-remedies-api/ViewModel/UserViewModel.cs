
using Microsoft.EntityFrameworkCore;
using System;

namespace vitality_home_remedies_api.ViewModel{
    public class UserViewModel{
        public  string Id { get; set; }
        public  string FirstName { get; set; }
         public  string LastName { get; set; }
         public  string Email { get; set; }
         public  string phonenumber { get; set; }

         public  string Password { get; set; }
         public  string IdNumber { get; set; }
         public string PasswordResetCode { get; set; }
         public DateTime? PasswordResetTime { get; set; }

    }
}
